DELIMITER $$
CREATE EVENT `The End`
  ON SCHEDULE EVERY 1 DAY STARTS '2020-11-20 00:00:00'
DO BEGIN
UPDATE Construction_site SET activity_type = 'is finish' where end_date > now();
END$$
DELIMITER ;