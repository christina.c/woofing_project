CREATE DEFINER=`simplon`@`localhost` TRIGGER `Woofing`.`Skills_AFTER_INSERT` AFTER INSERT ON `Skills` FOR EACH ROW
BEGIN
INSERT INTO skills (name) VALUES (NEW.name, 'New skills');
END