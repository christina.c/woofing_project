CREATE DEFINER=`simplon`@`localhost` PROCEDURE `ratio_voluntary`()
BEGIN
	
    DECLARE total_voluntary INT;
    DECLARE total_user INT;

    SELECT COUNT(*) INTO total_voluntary FROM Voluntary;
    SELECT COUNT(*) INTO total_user FROM User where hote = false;
    
    SELECT total_user * 100 / total_voluntary AS ratio_voluntary;

END